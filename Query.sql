-- 1. Create 5 Users
CREATE USER 'Sok Sothy_x'@'localhost' IDENTIFIED BY 'thy123';
CREATE USER 'Sok Makara'@'localhost' IDENTIFIED BY 'pink123';
CREATE USER 'Sok Chanda'@'localhost' IDENTIFIED BY 'Taaaax33';
CREATE USER 'Srun Srorn'@'localhost' IDENTIFIED BY 'bert45';
CREATE USER 'Sithol Pen'@'localhost' IDENTIFIED BY 'ggg67';
-- 2. Create 3 Roles
CREATE ROLE 'role_admin';
CREATE ROLE 'role_teacher';
CREATE ROLE 'role_student';
-- 3. Assign User to Role (at least 1 user having 2 roles)
GRANT 'role_admin' TO 'Sok Sothy_x'@'localhost';
GRANT 'role_teacher' TO 'Sok Sothy_x'@'localhost';
GRANT 'role_student' TO 'Sok Sothy_x'@'localhost';

GRANT 'role_teacher' TO 'Sok Makara'@'localhost';
GRANT 'role_student' TO 'Sok Makara'@'localhost';

GRANT 'role_admin' TO 'Sok Chanda'@'localhost';
GRANT 'role_student' TO 'Sok Chanda'@'localhost';

GRANT 'role_student' TO 'Srun Srorn'@'localhost';
GRANT 'role_teacher' TO 'Srun Srorn'@'localhost';

GRANT 'role_teacher' TO 'Sithol Pen'@'localhost';
GRANT 'role_student' TO 'Sithol Pen'@'localhost';

-- 4. Give privileges to the Role
GRANT ALL PRIVILEGES ON lab04.* TO 'role_admin';
GRANT SELECT, INSERT, UPDATE ON lab04.* TO 'role_teacher';
GRANT SELECT ON lab04.* TO 'role_student';

