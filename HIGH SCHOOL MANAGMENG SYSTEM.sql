use lap04;
create table Teachers(
Email varchar(50) unique not null ,
Address varchar(60),
Gender varchar(50),
Date_of_birth date,
T_subject varchar(50),
T_name varchar(50) not null,
teacher_ID  int  auto_increment primary key not NULL
);
create table Student(
Student_ID int auto_increment primary key,
Class_ID int  ,
Student_name varchar(40) not null,
email varchar (50) unique not null,
parent_phon int ,
Student_password int not null,
S_DOB date, 
 foreign key (Class_ID) references Class(Class_ID)
);
create table Grade (
Student_ID int,
score int ,
Grade_ID int ,
Date_Grede date,
FOREIGN KEY (Student_ID) REFERENCES students(Student_ID),
FOREIGN KEY (subject_ID) REFERENCES subjects(subject_ID)
);
create table Class (
class_name varchar(20),
Room_ID int primary key,
Class_ID int  ,
course_ID int ,
teacher_ID int 
);

create table Attendece(
Attendance_ID int primary key,
 Attendacne_date datetime,
statusd enum ('present' , 'absent' ,'excused') not null,
 Attendance_Student varchar(50),
 foreign key (Student_ID) references Student(Student_ID),
 foreign key (Class_ID) references Class(Class_ID)
);
create table lesson (
lesson_ID int auto_increment primary key ,
howework int ,
lesson_video int ,
quiz int ,
Class_ID int not null,
foreign key ( Class_ID) references Class(Class_ID)
);
create table timetable(
course_ID  int ,
teacher_ID int ,
lesson_ID int ,
Class_ID int ,
day_of_week ENUM('Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday') NOT NULL,
starttime date not null,
endtime time not null ,
foreign key (Class_ID) references Class(Class_ID),
foreign key (teacher_ID) references Teachers(teacher_ID),
foreign key (lesson_ID) references lesson(lesson_ID)
);

create table Admins (
Admin_name varchar(40),
Admin_ID int primary key,
Admin_tel int 

);
create table subjects (
subject_name varchar(50),
subject_ID int primary key

);
create table enrolment(
en_ID int primary key auto_increment,
en_date date,
Student_ID int ,
foreign key (Student_ID) references Student(Student),
foreign key (Class_ID) references Class(Class_ID)
);


