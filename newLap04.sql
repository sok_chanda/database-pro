create database newLab4;
use newLab4;


-- School Table
create table school(
    id int primary key auto_increment,
    address varchar(80) not null ,
    school_name varchar(45) not null ,
    school_principal varchar(45) not null
);
-- Table enrollment
create table enrollments(
    id int primary key auto_increment,
    enroll_date date
);
-- Table students
create table students (
    id int primary key auto_increment ,
    name varchar(100) not null,
    email varchar(50) not null,
    password varchar(255) not null default 'student@bati',
    stu_dob date not null,
    parents_phone varchar(10),
    gender varchar(6)
);
-- Table teachers
create table teachers(
    id int primary key auto_increment,
    name varchar(50),
    email varchar(50) not null ,
    password varchar(255) not null ,
    gender varchar(6) not null,
    date_of_birth date not null ,
    t_subject varchar(50) not null,
    phone_number varchar(10),
    school_id int,
    foreign key (school_id) references school(id)
);

-- Table Admin
# create table admins(
#     id int primary key auto_increment,
#     username varchar(50) not null ,
#     phone_number varchar(10) not null,
#     email varchar(100),
#     password varchar(255) not null,
#     student_id int,
#     lesson_id int,
#     enrollment_id int,
#     teacher_id int,
#     foreign key (student_id) references students(id),
#     foreign key (lesson_id) references lesson(id),
#     foreign key (enrollment_id) references enrollments(id),
#     foreign key (teacher_id) references teachers(id)
# );

-- Table class
create table class(
    id int primary key auto_increment,
    class_name varchar(50),
    student_id int,
    teacher_id int,
    foreign key (student_id) references students(id),
    foreign key (teacher_id) references teachers(id),
    subject_id int,
    foreign key (student_id) references subject(id),
    start_date datetime,
    end_date datetime
);

-- Table lesson
create table lesson(
    id int primary key auto_increment,
    title varchar(255),
    release_date date not null,
    files varchar(255),
    quiz varchar(255),
    homework varchar(255),
    video varchar(255),
    teacher_id int,
    foreign key (teacher_id) references teachers(id)
);

-- Table subject
create table subject(
    id int primary key auto_increment,
    name varchar(50),
    teacher_id int,
    foreign key (teacher_id) references teachers(id)
);
# -- Table Grade
# create table grade(
#     id int primary key auto_increment,
#     foreign key (subject_id) references subject(id),
#     score int(12)
# );

-- Table Attendance
create table attendance(
    id int primary key auto_increment,
    attendance_date date not null ,
    present int,
    late int,
    absent int default 0,
    student_id int ,
    foreign key (student_id) references students(id),
    class_id int,
    foreign key (class_id) references class(id)
);

-- Table Exam
create table exam(
    id int primary key auto_increment,
    start_date datetime not null,
    end_date datetime not null ,
    name varchar(45)
);

-- result exam
create table resultExam(
    id int primary key auto_increment,
    score int not null default 0,
    student_id int,
    foreign key (student_id) references students(id),
    exam_id int,
    foreign key (exam_id) references exam(id)
);


-- dml

-- insert data into school table
insert into school (address,school_name,school_principal)
values ('Bati district Takeo Province','BunranyHunsenBati','Sok Sothy');
select * from school where id=1;
-- insert data into students
# DATE - format YYYY-MM-DD
# DATETIME - format: YYYY-MM-DD HH:MI:SS
insert into students (name,email,password,stu_dob,parents_phone,gender)
values
('rong sovnaroth','sovanaroth.rong@student.bati.edu.kh','123','2007-05-16','016538687','male'),
('sok sothy','sothy.sok@student.bati.edu.kh','123','2005-10-20','012586478','male'),
('ny theavy','theavy.ny@student.bati.edu.kh','123','2004-07-25','096458745','female'),
('keo sokuntheary','sokuntheary.keo@student.bati.edu.kh','123','2004-04-17','069785654','female'),
('nget darapich','darapich.nget@student.bati.edu.kh','123','2005-05-30','096475837','male');

-- insert data into table Teacher
insert into teachers(name,email,password,gender,date_of_birth,t_subject,phone_number,school_id)
values
('hout saravuth','saravuth.hout@bati.edu.kh','123','male','1989-11-05','maths','097566477',1),
('so savourn','savourn.so@bati.edu.kh','123','female','1982-01-07','maths','012456986',1),
('doung bol','bol.doung@bati.edu.kh','123','male','1970-10-27','physics','097536123',1),
('seu somnang','somnang.seu@bati.edu.kh','123','male','1960-06-15','chemistry','096751369',1),
('sang phearom','phearom.sang@bati.edu.kh','123','male','1980-12-01','biology','070856533',1),
('seng muny','muny.seng@bati.edu.kh','123','male','1990-10-30','english','097566477',1),
('chea lalin','lalin.chea@bati.edu.kh','123','male','1986-10-09','khmer','097566457',1),
('khev chhoukrath','chhoukrath.khev@bati.edu.kh','123','female','1995-10-21','history','095666477',1);

-- insert into subject
insert into subject(name, teacher_id)
values
('maths',1),
('maths',2),
('physics',3),
('chemistry',4),
('biology',5),
('english',6),
('khmer',7),
('history',8);

-- insert into table class
insert into class(class_name, student_id, teacher_id, subject_id, start_date, end_date)
values
('12A',3,1,1,'2022-02-25 07:00:00','2022-02-25 09:00:00'),
('12A',2,2,2,'2022-02-25 09:00:00','2022-02-25 11:00:00'),
('9F',1,3,3,'2022-02-25 02:00:00','2022-02-25 03:00:00'),
('11A',4,4,4,'2022-02-25 03:00:00','2022-02-25 04:00:00');

-- insert into lesson table
insert into lesson(title, release_date, files, quiz, homework, video, teacher_id)
values
('linear algebra','2022-02-24','linear_algebra.pdf','','','',1),
('force','2022-02-26','newton_2nd_law.pdf','','','url',3);


select * from teachers;

-- user previlege

-- 1. create 5 users
create user 'sokmakara'@'localhost' identified by 'mkr16';
create user 'hongkimnith'@'localhost' identified by 'nith009';
create user 'houtsaravuth'@'localhost' identified by 'vuth99';
create user 'keosokuntheary'@'localhost' identified by 'ahrya88';
create user 'thongvathana'@'localhost' identified by 'vath24';
create user 'soksothy'@'localhost' identified by 'thy9999';
create user 'limtaihai'@'localhost' identified by 'haismos';

-- 2. create 3 roles

create role bati_student_role;
create role bati_teacher_role;
create role bati_admin_role;

-- 3. assign user to role
grant bati_teacher_role to 'houtsaravuth'@'localhost';
grant bati_student_role to 'sokmakara'@'localhost';








-- 4. give previleges to the role
grant show databases on *.* to 'bati_teacher_role';
grant show databases on *.* to bati_student_role;
grant show databases on *.* to bati_admin_role;
show grants for bati_teacher_role;

-- give permission to teacher
grant select,insert,delete,update on newlab4.teachers to 'bati_teacher_role';
grant select,insert,delete,update on newlab4.lesson to 'bati_teacher_role';

-- give permission to student
grant select on newlab4.* to bati_student_role;
grant update(name,password) on newlab4.students to bati_student_role;

-- giver permission to admin
grant all privileges on newlab4.* to bati_admin_role;
grant select,insert,update,delete on newlab4.* to bati_admin_role;

-- 5. ravoke a permission from a role
revoke update(name,password) on newlab4.students from bati_student_role;

-- 6. drop 1 role
drop role bati_student_role;

-- 7. rename the role
-- i will rename of "bati_teacher_role" to "lecturer_bati"
-- creat new role first 
create role lecturer_bati;

-- show all permission of the old role
show grants for bati_teacher_role;

-- give permission the same as the old role
grant show databases on *.* to 'lecturer_bati';
grant select,insert,delete,update on newlab4.teachers to 'lecturer_bati';
grant select,insert,delete,update on newlab4.lesson to 'lecturer_bati';

-- drop old role which we want to rename on
drop role bati_teacher_role;

-- assign the user to the role which we are renamed
grant lecturer_bati to 'houtsaravuth'@'localhost';

-- rename will be successful

-- 8 Set limit queries for a user 5 queries per hour
alter user 'sokmakara'@'localhost' WITH MAX_QUERIES_PER_HOUR 5;
alter user 'houtsaravuth'@'localhost' WITH MAX_QUERIES_PER_HOUR 5;
alter user 'keosokuntheary'@'localhost' WITH MAX_QUERIES_PER_HOUR 5;
alter user 'soksothy'@'localhost'  WITH MAX_QUERIES_PER_HOUR 5;