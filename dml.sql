-- insert data into school table
insert into school (address,school_name,school_principal)
values ('Bati district Takeo Province','BunranyHunsenBati','Sok Sothy');
select * from school where id=1;
-- insert data into students
# DATE - format YYYY-MM-DD
# DATETIME - format: YYYY-MM-DD HH:MI:SS
insert into students (name,email,password,stu_dob,parents_phone,gender)
values
('rong sovnaroth','sovanaroth.rong@student.bati.edu.kh','123','2007-05-16','016538687','male'),
('sok sothy','sothy.sok@student.bati.edu.kh','123','2005-10-20','012586478','male'),
('ny theavy','theavy.ny@student.bati.edu.kh','123','2004-07-25','096458745','female'),
('keo sokuntheary','sokuntheary.keo@student.bati.edu.kh','123','2004-04-17','069785654','female'),
('nget darapich','darapich.nget@student.bati.edu.kh','123','2005-05-30','096475837','male');

-- insert data into table Teacher
insert into teachers(name,email,password,gender,date_of_birth,t_subject,phone_number,school_id)
values
('hout saravuth','saravuth.hout@bati.edu.kh','123','male','1989-11-05','maths','097566477',1),
('so savourn','savourn.so@bati.edu.kh','123','female','1982-01-07','maths','012456986',1),
('doung bol','bol.doung@bati.edu.kh','123','male','1970-10-27','physics','097536123',1),
('seu somnang','somnang.seu@bati.edu.kh','123','male','1960-06-15','chemistry','096751369',1),
('sang phearom','phearom.sang@bati.edu.kh','123','male','1980-12-01','biology','070856533',1),
('seng muny','muny.seng@bati.edu.kh','123','male','1990-10-30','english','097566477',1),
('chea lalin','lalin.chea@bati.edu.kh','123','male','1986-10-09','khmer','097566457',1),
('khev chhoukrath','chhoukrath.khev@bati.edu.kh','123','female','1995-10-21','history','095666477',1);

-- insert into subject
insert into subject(name, teacher_id)
values
('maths',1),
('maths',2),
('physics',3),
('chemistry',4),
('biology',5),
('english',6),
('khmer',7),
('history',8);

-- insert into table class
insert into class(class_name, student_id, teacher_id, subject_id, start_date, end_date)
values
('12A',3,1,1,'2022-02-25 07:00:00','2022-02-25 09:00:00'),
('12A',2,2,2,'2022-02-25 09:00:00','11:00:00'),
('9F',1,3,3,'2022-02-25 02:00:00','2022-02-25 03:00:00'),
('11A',4,4,4,'2022-02-25 03:00:00','2022-02-25 04:00:00');

-- insert into lesson table
insert into lesson(title, release_date, files, quiz, homework, video, teacher_id)
values
('linear algebra','2022-02-24','linear_algebra.pdf','','','',1),
('force','2022-02-26','newton_2nd_law.pdf','','','url',3);
