
-- School Table
create table school(
    id int primary key auto_increment,
    address varchar(80) not null ,
    school_name varchar(45) not null ,
    school_principal varchar(45) not null
);
-- Table enrollment
create table enrollments(
    id int primary key auto_increment,
    enroll_date date
);
-- Table students
create table students (
    id int primary key auto_increment ,
    name varchar(100) not null,
    email varchar(50) not null,
    password varchar(255) not null default 'student@bati',
    stu_dob date not null,
    parents_phone varchar(10),
    gender varchar(6)
);
-- Table teachers
create table teachers(
    id int primary key auto_increment,
    name varchar(50),
    email varchar(50) not null ,
    password varchar(255) not null ,
    gender varchar(6) not null,
    date_of_birth date not null ,
    t_subject varchar(50) not null,
    phone_number varchar(10),
    school_id int,
    foreign key (school_id) references school(id)
);

-- Table Admin
# create table admins(
#     id int primary key auto_increment,
#     username varchar(50) not null ,
#     phone_number varchar(10) not null,
#     email varchar(100),
#     password varchar(255) not null,
#     student_id int,
#     lesson_id int,
#     enrollment_id int,
#     teacher_id int,
#     foreign key (student_id) references students(id),
#     foreign key (lesson_id) references lesson(id),
#     foreign key (enrollment_id) references enrollments(id),
#     foreign key (teacher_id) references teachers(id)
# );

-- Table class
create table class(
    id int primary key auto_increment,
    class_name varchar(50),
    student_id int,
    teacher_id int,
    foreign key (student_id) references students(id),
    foreign key (teacher_id) references teachers(id),
    subject_id int,
    foreign key (student_id) references subject(id),
    start_date datetime,
    end_date datetime
);

-- Table lesson
create table lesson(
    id int primary key auto_increment,
    title varchar(255),
    release_date date not null,
    files varchar(255),
    quiz varchar(255),
    homework varchar(255),
    video varchar(255),
    teacher_id int,
    foreign key (teacher_id) references teachers(id)
);

-- Table subject
create table subject(
    id int primary key auto_increment,
    name varchar(50),
    teacher_id int,
    foreign key (teacher_id) references teachers(id)
);
# -- Table Grade
# create table grade(
#     id int primary key auto_increment,
#     foreign key (subject_id) references subject(id),
#     score int(12)
# );

-- Table Attendance
create table attendance(
    id int primary key auto_increment,
    attendance_date date not null ,
    present int,
    late int,
    absent int default 0,
    student_id int ,
    foreign key (student_id) references students(id),
    class_id int,
    foreign key (class_id) references class(id)
);

-- Table Exam
create table exam(
    id int primary key auto_increment,
    start_date datetime not null,
    end_date datetime not null ,
    name varchar(45)
);

-- result exam
create table resultExam(
    id int primary key auto_increment,
    score int not null default 0,
    student_id int,
    foreign key (student_id) references students(id),
    exam_id int,
    foreign key (exam_id) references exam(id)
);